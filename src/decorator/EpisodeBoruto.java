package decorator;

import javax.swing.ImageIcon;

import UI.appUI;

public class EpisodeBoruto implements EpisodeDecorator {

	@Override
	public ImageIcon background() {
		return new ImageIcon(appUI.class.getResource("/img/boruto.jpg"));
		
	}

}
