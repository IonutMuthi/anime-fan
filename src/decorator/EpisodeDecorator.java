package decorator;

import javax.swing.ImageIcon;

public interface EpisodeDecorator {
   public ImageIcon background();
}
