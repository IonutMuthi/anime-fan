package decorator;

import javax.swing.ImageIcon;

import UI.appUI;

public class Episode91Days implements EpisodeDecorator {

	@Override
	public ImageIcon background() {
		return new ImageIcon(appUI.class.getResource("/img/91Days.jpg"));
		
	}

}
