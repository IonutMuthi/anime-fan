package decorator;

import javax.swing.ImageIcon;

import UI.appUI;

public class EpisodeAkame implements EpisodeDecorator {

	@Override
	public ImageIcon background() {
		return new ImageIcon(appUI.class.getResource("/img/akame.jpg"));
		
	}

}
