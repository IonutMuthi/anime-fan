package UI;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;

import dao.AnimeDAOImplement;
import daoInterface.AnimeDAO;
import decorator.EpisodeDecorator;
import factory.EpisodeFactory;
import model.Anime;
import model.Episode;
import model.Gen;
import strategy.Strategy;

public class appAdminUI extends JFrame {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JPanel homePane;

	private JPanel animePane;
	private JPanel selectedAnimePane;
	private JPanel episodePane;
	private JPanel singUpPane;
	private JPanel managePane;
	
	
	private JTextField txtUsername;
	private JLabel lblScore;
	private JLabel lblPassword;
	private JLabel lblUserName;

	private JPasswordField pwdPass;
	private JButton btnBack;
	private JTextField textField;
	private JPasswordField passwordField;
	private JLabel lblAge;
	private JLabel lblEmail;
	private JPasswordField passwordField_1;
	private JTextField textField_1;
    
    private AnimeDAO animeDB=new AnimeDAOImplement();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					String local=null;
					appAdminUI frame = new appAdminUI(null, local);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	public appAdminUI(Connection con,String username) throws SQLException {
	     home(con,username);
	}
	
	
	private void menu(JPanel panel,Connection con,String username)throws SQLException{
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 434, 40);
		panel.add(menuBar);
		
		JMenu mnHome = new JMenu("Home");
		mnHome.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				panel.setVisible(false);
				try {
					home(con,username);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		menuBar.add(mnHome);
		
		JMenu mnAnimeList = new JMenu("Anime List");
		mnAnimeList.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					panel.setVisible(false);
					anime(con,username);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		menuBar.add(mnAnimeList);
		
		JMenu mnSingUp = new JMenu("Add client");
		mnSingUp.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				panel.setVisible(false);
				singUp(con, username);
			}
		});
		menuBar.add(mnSingUp);
		
		JMenu mnLogIn = new JMenu(username);
		
		menuBar.add(mnLogIn);
		
		JMenu mnManage = new JMenu("Manage Animes");
		mnManage.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				panel.setVisible(false);
				try {
					panel.setVisible(false);
					manage(con, username);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		menuBar.add(mnManage);
		
		
		
	
	}
	
	
	private void manage(Connection con,String username) throws SQLException{
		setTitle("Anime manage");
		setBounds(100, 100, 450, 300);
		managePane=new JPanel();
		managePane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(managePane);
		managePane.setLayout(null);
		
		menu(managePane,con, username);

		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(10, 40, 71, 14);
		managePane.add(lblName);
		
		
		textField = new JTextField();
		textField.setBounds(100, 40, 86, 20);
		managePane.add(textField);
		textField.setColumns(10);
		
		JCheckBox chckbxR = new JCheckBox("R");
		chckbxR.setBounds(6, 56, 97, 23);
		managePane.add(chckbxR);
		
		
		JCheckBox chckbxAction = new JCheckBox("Action");
		chckbxAction.setBounds(100, 84, 71, 23);
		managePane.add(chckbxAction);
		
		JLabel lblGen = new JLabel("Gen:");
		lblGen.setBounds(10, 88, 46, 14);
		managePane.add(lblGen);
		
		JCheckBox chckbxAdventure = new JCheckBox("Adventure");
		chckbxAdventure.setBounds(100, 110, 97, 23);
		managePane.add(chckbxAdventure);
		
		JCheckBox chckbxComedy = new JCheckBox("Comedy");
		chckbxComedy.setBounds(100, 140, 97, 23);
		managePane.add(chckbxComedy);
		
		JCheckBox chckbxDrama = new JCheckBox("Drama");
		chckbxDrama.setBounds(10, 110, 97, 23);
		managePane.add(chckbxDrama);
		
		JCheckBox chckbxFantasy = new JCheckBox("Fantasy");
		chckbxFantasy.setBounds(10, 140, 97, 23);
		managePane.add(chckbxFantasy);
		
		JCheckBox chckbxHorror = new JCheckBox("Horror");
		chckbxHorror.setBounds(100, 166, 97, 23);
		managePane.add(chckbxHorror);
		
		JCheckBox chckbxMecha = new JCheckBox("Mecha");
		chckbxMecha.setBounds(10, 166, 97, 23);
		managePane.add(chckbxMecha);
		
		JCheckBox chckbxRomance = new JCheckBox("Romance");
		chckbxRomance.setBounds(100, 192, 97, 23);
		managePane.add(chckbxRomance);
		
		JCheckBox chckbxMystery = new JCheckBox("Mystery");
		chckbxMystery.setBounds(10, 192, 97, 23);
		managePane.add(chckbxMystery);
		
		ArrayList<Gen> gens=new ArrayList<>();
		
		if(chckbxAction.isSelected()){
			gens.add(Gen.ACTION);
		}
		if(chckbxAdventure.isSelected()){
			gens.add(Gen.ADVENTURE);
		}
		if(chckbxComedy.isSelected()){
			gens.add(Gen.COMEDY);
		}
		if(chckbxDrama.isSelected()){
			gens.add(Gen.DRAMA);
		}
		if(chckbxFantasy.isSelected()){
			gens.add(Gen.FANTASY);
		}
		if(chckbxHorror.isSelected()){
			gens.add(Gen.HORROR);
		}
		if(chckbxMecha.isSelected()){
			gens.add(Gen.MECHA);
		}
		if(chckbxRomance.isSelected()){
			gens.add(Gen.ROMANCE);
		}
		if(chckbxMystery.isSelected()){
			gens.add(Gen.MYSTERY);
		}
		
		ArrayList<Episode> episodes=null;
		
		JButton btnAddAnime = new JButton("Add anime");
		btnAddAnime.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				try {
					animeDB.insertAnime(new Anime(textField.getText(),0.0,gens,chckbxR.isSelected(),episodes), con);
					Strategy strategy=new Strategy();
					strategy.sugest(textField.getText());
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnAddAnime.setBounds(10, 227, 116, 23);
		managePane.add(btnAddAnime);
		
		JTextField textField_2 = new JTextField();
		textField_2.setBounds(300, 40, 86, 20);
		managePane.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblEpisodeName = new JLabel("Episode name:");
		lblEpisodeName.setBounds(205, 40, 85, 14);
		managePane.add(lblEpisodeName);
		
		JLabel lblAnimeName = new JLabel("Anime name:");
		lblAnimeName.setBounds(205, 80, 86, 14);
		managePane.add(lblAnimeName);
		
		JTextField textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(300, 80, 86, 20);
		managePane.add(textField_3);
		
		JButton btnAddEpisode = new JButton("Add episode");
		btnAddEpisode.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				try {
					animeDB.insertEpisode(textField_3.getText(), textField_2.getText(), con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnAddEpisode.setBounds(270, 140, 116, 23);
		managePane.add(btnAddEpisode);
	}
	
	
	private void home(Connection con, String username) throws SQLException{
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Anime fan");
		setBounds(100, 100, 450, 300);
		homePane = new JPanel();
		homePane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		setContentPane(homePane);
		homePane.setLayout(null);
		
		menu(homePane,con, username);
		
		List list = new List();
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					System.out.println(list.getSelectedItem());
					Episode episode;
					try {
						episode = animeDB.findEpisodeByName(list.getSelectedItem(),con);
						homePane.setVisible(false);
						getEpisode(episode,con, username,animeDB.findByEpisode(episode.getName(), con).getName());
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				  }
				
			}
		});
		list.setBounds(10, 50, 110, 190);
		getLast5(list,con);

		homePane.add(list);
		List list2 = new List();
		list2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					System.out.println(list.getSelectedItem());
					Anime anime;
					try {
						anime = animeDB.findByName(list2.getSelectedItem(),con);
						homePane.setVisible(false);
						selectedAnime(anime,con, username);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				  }
				
			}

			
		});
		list2.setBounds(150, 50, 110, 190);
		getAnimes(list2,con,username);

		homePane.add(list2);

	}
	
	private void getAnimes(List list, Connection con, String username) throws SQLException {
		for(Anime a :animeDB.getUserAnime(username, con)){
			list.add(a.getName());
		}
		
	}
	
   private void getLast5(List list,Connection con) throws SQLException{
		
		for(Episode a :animeDB.getLast5Episodes(con)){
			list.add(a.getName());
		}
		
	}
	
	
	
   void singUp(Connection con,String username){
		
		
		setTitle("Sing up");
	//	setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 340);
		singUpPane = new JPanel();
		singUpPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(singUpPane);
		singUpPane.setLayout(null);

	
	
		
		JButton btnAutentification = new JButton("Add");
		btnAutentification.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnAutentification.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				
	          try {
	        	        // new acc validator email username 
	        		  
	        	        }  catch (Exception e1) {
					e1.printStackTrace();
					
				}
	          
			}
			
		});
		
		
		
		
		lblUserName = new JLabel("User name:");
		lblUserName.setBounds(40, 60, 120, 14);
		singUpPane.add(lblUserName);
		
		btnAutentification.setBounds(100, 237, 200, 23);
		singUpPane.add(btnAutentification);
		
		

		
		txtUsername = new JTextField();
		txtUsername.setBounds(160, 50, 110, 30);
		singUpPane.add(txtUsername);
		txtUsername.setColumns(10);
		
		lblPassword = new JLabel("Password:");
		lblPassword.setBounds(40, 100, 120, 14);
		singUpPane.add(lblPassword);
		
		pwdPass = new JPasswordField();
		pwdPass.setText("");
		pwdPass.setBounds(160, 90, 110, 30);
		singUpPane.add(pwdPass);
		
		btnBack = new JButton("back");
		btnBack.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				singUpPane.setVisible(false);
				try {
					home(con,username);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnBack.setBounds(160, 267, 110, 23);
		singUpPane.add(btnBack);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(160, 125, 110, 30);
		singUpPane.add(textField);
		
		JLabel lblRepetPassword = new JLabel("Confirm password");
		lblRepetPassword.setBounds(40, 135, 120, 14);
		singUpPane.add(lblRepetPassword);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(40, 21, 120, 14);
		singUpPane.add(lblName);
		
		passwordField = new JPasswordField();
		passwordField.setText("");
		passwordField.setBounds(160, 11, 110, 30);
		singUpPane.add(passwordField);
		
		lblAge = new JLabel("Age:");
		lblAge.setBounds(40, 211, 120, 14);
		singUpPane.add(lblAge);
		
		lblEmail = new JLabel("Email:");
		lblEmail.setBounds(40, 176, 120, 14);
		singUpPane.add(lblEmail);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setText("");
		passwordField_1.setBounds(160, 166, 110, 30);
		singUpPane.add(passwordField_1);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(160, 201, 110, 30);
		singUpPane.add(textField_1);
		

	
	}

	private void anime(Connection con,String username) throws SQLException{
		
		setTitle("Anime");
		//setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		animePane = new JPanel();
		animePane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(animePane);
		animePane.setLayout(null);
		
		menu(animePane,con,username);
		
		List list = new List();
		list.setBounds(10, 50, 110, 190);
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					System.out.println(list.getSelectedItem());
					try {
						Anime a=animeDB.findByName(list.getSelectedItem(),con);
						animePane.setVisible(false);
						selectedAnime(a,con,username);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				  }
				
			}
		});
		getList(list,con);
		
		animePane.add(list);
	}
	
   private void getList(List list,Connection con) throws SQLException{
		
		for(Anime a :animeDB.getAnimes(con)){
			list.add(a.getName());
		}
		
	}
	
	private void selectedAnime(Anime anime,Connection con,String username) throws SQLException{
		setTitle("Anime");
	//	setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 330);
		selectedAnimePane = new JPanel();
		selectedAnimePane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(selectedAnimePane);
		selectedAnimePane.setLayout(null);
		
		menu(selectedAnimePane,con,username);
		
		JLabel lblAnime = DefaultComponentFactory.getInstance().createTitle(anime.getName());
		lblAnime.setBounds(10,45, 88, 14);
		selectedAnimePane.add(lblAnime);
		
		
		
		List list = new List();
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					System.out.println(list.getSelectedItem());
					Episode episode;
					try {
						episode = animeDB.findEpisodeByName(list.getSelectedItem(),con);
						selectedAnimePane.setVisible(false);
						getEpisode(episode,con, username,anime.getName());
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				  }
				
			}
		});
		list.setBounds(10, 100, 120, 165);
		getList(list,anime);
		
	 
		
		selectedAnimePane.add(list);
		
		 lblScore = new JLabel("");
		lblScore.setBounds(150, 145, 97, 14);
		lblScore.setText("Score: "+anime.getRating());
		selectedAnimePane.add(lblScore);
		
		
		JCheckBox chckbxFollow = new JCheckBox("Follow");
		for(Anime a :animeDB.getUserAnime(username,con)){
			if(a.getName().equals(anime.getName())){
				chckbxFollow.setSelected(true);
			}
		}
		chckbxFollow.setBounds(150, 165, 97, 23);
		
		selectedAnimePane.add(chckbxFollow);
		
		rate(anime,username,con);
      
		
		JLabel lblRate = new JLabel("  1    2     3    4     5");
		lblRate.setBounds(150, 215, 101, 14);
		selectedAnimePane.add(lblRate);
		
		JLabel lblGen = new JLabel(getGen(anime));
		lblGen.setBounds(10, 55, 264, 39);
		selectedAnimePane.add(lblGen);
		
		if(anime.getR()){
		JLabel lblR = new JLabel("R");
		lblR.setBounds(150, 120, 46, 14);
		selectedAnimePane.add(lblR);
		}		
		
	}
	
	
	private void rate(Anime anime,String username,Connection con){
		JRadioButton rdbtnS1 = new JRadioButton("");   // combo box better ? 
		JRadioButton rdbtnS2 = new JRadioButton("");
		JRadioButton rdbtnS3 = new JRadioButton("");
		JRadioButton rdbtnS4 = new JRadioButton("");
		JRadioButton rdbtnS5 = new JRadioButton("");
		
		rdbtnS1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				anime.setRate(1);
				rdbtnS2.setSelected(false);
				rdbtnS3.setSelected(false);
				rdbtnS4.setSelected(false);
				rdbtnS5.setSelected(false);
				lblScore.setText("Score: "+anime.getRating());
				try {
					
					animeDB.updateRating(anime.getName(),  Double.parseDouble(anime.getRating()), con);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		rdbtnS1.setBounds(150, 190, 21, 23);
		selectedAnimePane.add(rdbtnS1);
		
		
		rdbtnS2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				rdbtnS1.setSelected(true);
				rdbtnS3.setSelected(false);
				rdbtnS4.setSelected(false);
				rdbtnS5.setSelected(false);
				lblScore.setText("Score: "+anime.getRating());
				anime.setRate(2);
				try {
					animeDB.updateRating(anime.getName(),  Double.parseDouble(anime.getRating()), con);
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		rdbtnS2.setBounds(170, 190, 21, 23);
		selectedAnimePane.add(rdbtnS2);
		
	
		rdbtnS3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				anime.setRate(3);
				rdbtnS1.setSelected(true);
				rdbtnS2.setSelected(true);
				rdbtnS4.setSelected(false);
				rdbtnS5.setSelected(false);
				lblScore.setText("Score: "+anime.getRating());
				try {
					animeDB.updateRating(anime.getName(),  Double.parseDouble(anime.getRating()), con);
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		rdbtnS3.setBounds(190, 190, 21, 23);
		selectedAnimePane.add(rdbtnS3);
		
		
		rdbtnS4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				rdbtnS1.setSelected(true);
				rdbtnS2.setSelected(true);
				rdbtnS3.setSelected(true);
				rdbtnS5.setSelected(false);
				anime.setRate(4);
				lblScore.setText("Score: "+anime.getRating());
				try {
					animeDB.updateRating(anime.getName(),  Double.parseDouble(anime.getRating()), con);
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		rdbtnS4.setBounds(210, 190, 21, 23);
		selectedAnimePane.add(rdbtnS4);
		
		
		rdbtnS5.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				rdbtnS1.setSelected(true);
				rdbtnS2.setSelected(true);
				rdbtnS3.setSelected(true);
				rdbtnS4.setSelected(true);
				anime.setRate(5);
				lblScore.setText("Score: "+anime.getRating());
				try {
					animeDB.updateRating(anime.getName(), Double.parseDouble(anime.getRating()), con);
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		rdbtnS5.setBounds(230, 190, 21, 23);
		selectedAnimePane.add(rdbtnS5);
	}
	
	private void getList(List list,Anime a) throws SQLException{
		for(Episode e :a.getEpsisodes()){
			list.add(e.getName());
			
		}
		}
	
	private String getGen(Anime a){
		String gen="Gen: ";
		for(Gen g:a.getGens()){
			gen+=g.name().toLowerCase()+",";
		}
		return gen;
	}
	
	private void getEpisode(Episode ep,Connection con,String username,String anime) throws SQLException{
		setTitle("Episode");
	//	setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 640, 500);
		
		episodePane = new JPanel();
		episodePane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(episodePane);
		episodePane.setLayout(null);
		menu(episodePane,con,username);
		JLabel lblName = new JLabel(ep.getName());
		lblName.setBounds(10, 45, 80, 14);
		episodePane.add(lblName);
 
		 //set name si img in fct de ep

		EpisodeFactory epBack=new EpisodeFactory();
		EpisodeDecorator decorator= epBack.selectEpisode(anime);
		ImageIcon img=decorator.background();
		
		JLabel lblEp = new JLabel("");
		lblEp.setIcon(img);		
		lblEp.setBounds(10, 60, 600, 400);	
		episodePane.add(lblEp);
	}


}
