package UI;


import java.awt.EventQueue;
import java.awt.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;

import Start.Start;
import dao.AnimeDAOImplement;
import dao.UserDAOImplement;
import daoInterface.AnimeDAO;
import daoInterface.UserDAO;
import decorator.EpisodeDecorator;
import factory.EpisodeFactory;
import model.Administrator;
import model.Anime;
import model.Client;
import model.Episode;
import model.Gen;
import model.User;

import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JMenu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.SQLException;

public class appUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel homePane;
	private JPanel logPane;
	private JPanel singUpPane;
	private JPanel animePane;
	private JPanel selectedAnimePane;
	private JPanel episodePane;
	
	
	private JTextField txtUsername;
	private JLabel lblPassword;
	private JLabel lblUserName;

	

	private String username,password;
	private JPasswordField pwdPass;
	private JButton btnBack;
	private JTextField textField;
	private JPasswordField passwordField;
	private JLabel lblAge;
	private JLabel lblEmail;
	private JPasswordField passwordField_1;
	private JTextField textField_1;
	private JLabel lblScore;
    
    private UserDAO connection=new UserDAOImplement();
    private AnimeDAO animeDB=new AnimeDAOImplement();
	


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					appUI frame = new appUI(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param con 
	 * @throws SQLException 
	 */
	public appUI(Connection con) throws SQLException {
	     home(con);
	}
	
	
	private void menu(JPanel panel,Connection con){
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 434, 40);
		panel.add(menuBar);
		
		JMenu mnHome = new JMenu("Home");
		mnHome.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				panel.setVisible(false);
				try {
					home(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		menuBar.add(mnHome);
		
		JMenu mnAnimeList = new JMenu("Anime List");
		mnAnimeList.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					panel.setVisible(false);
					anime(con);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		menuBar.add(mnAnimeList);
		
		JMenu mnLogIn = new JMenu("Login ");
		mnLogIn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				panel.setVisible(false);
				login(con);
				
			}
		});
		menuBar.add(mnLogIn);
		
		JMenu mnSingUp = new JMenu("Sing up");
		mnSingUp.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				panel.setVisible(false);
				singUp(con);
			}
		});
		menuBar.add(mnSingUp);
	}
	
	private void home(Connection con) throws SQLException{
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Anime fan");
		setBounds(100, 100, 450, 300);
		homePane = new JPanel();
		homePane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		setContentPane(homePane);
		homePane.setLayout(null);
		
		menu(homePane,con);
		
		List list = new List();
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					System.out.println(list.getSelectedItem());
					Episode episode;
					try {
						episode = animeDB.findEpisodeByName(list.getSelectedItem(),con);
						homePane.setVisible(false);
						getEpisode(episode,con,animeDB.findByEpisode(episode.getName(), con).getName());
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				  }
				
			}
		});
		list.setBounds(10, 50, 110, 190);
		getLast5(list,con);

		homePane.add(list);

	}
	
    private void getLast5(List list,Connection con) throws SQLException{
		
		for(Episode a :animeDB.getLast5Episodes(con)){
			list.add(a.getName());
		}
		
	}
    
    
    void singUp(Connection con){
		
		
		setTitle("Sing up");
	//	setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 340);
		singUpPane = new JPanel();
		singUpPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(singUpPane);
		singUpPane.setLayout(null);

	
	
		
		JButton btnAutentification = new JButton("Sing up");
		btnAutentification.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnAutentification.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				
	          try {
	        	        // new acc validator email username 
	        		  
	        	        }  catch (Exception e1) {
					e1.printStackTrace();
					
				}
	          
			}
			
		});
		
		
		
		
		lblUserName = new JLabel("User name:");
		lblUserName.setBounds(40, 60, 120, 14);
		singUpPane.add(lblUserName);
		
		btnAutentification.setBounds(100, 237, 200, 23);
		singUpPane.add(btnAutentification);
		
		

		
		txtUsername = new JTextField();
		txtUsername.setBounds(160, 50, 110, 30);
		singUpPane.add(txtUsername);
		txtUsername.setColumns(10);
		
		lblPassword = new JLabel("Password:");
		lblPassword.setBounds(40, 100, 120, 14);
		singUpPane.add(lblPassword);
		
		pwdPass = new JPasswordField();
		pwdPass.setText("");
		pwdPass.setBounds(160, 90, 110, 30);
		singUpPane.add(pwdPass);
		
		btnBack = new JButton("back");
		btnBack.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				singUpPane.setVisible(false);
				try {
					home(con);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnBack.setBounds(160, 267, 110, 23);
		singUpPane.add(btnBack);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(160, 125, 110, 30);
		singUpPane.add(textField);
		
		JLabel lblRepetPassword = new JLabel("Confirm password");
		lblRepetPassword.setBounds(40, 135, 120, 14);
		singUpPane.add(lblRepetPassword);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(40, 21, 120, 14);
		singUpPane.add(lblName);
		
		passwordField = new JPasswordField();
		passwordField.setText("");
		passwordField.setBounds(160, 11, 110, 30);
		singUpPane.add(passwordField);
		
		lblAge = new JLabel("Age:");
		lblAge.setBounds(40, 211, 120, 14);
		singUpPane.add(lblAge);
		
		lblEmail = new JLabel("Email:");
		lblEmail.setBounds(40, 176, 120, 14);
		singUpPane.add(lblEmail);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setText("");
		passwordField_1.setBounds(160, 166, 110, 30);
		singUpPane.add(passwordField_1);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(160, 201, 110, 30);
		singUpPane.add(textField_1);
		

	
	}
	
	private void login(Connection con){
	
		setTitle("Login");
		JButton btnBack;
		setBounds(100, 100, 450, 300);
		
		logPane = new JPanel();
		logPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(logPane);
		logPane.setLayout(null);
		
		JButton btnAutentification = new JButton("Autentification");
		btnAutentification.addMouseListener(new MouseAdapter() {
			
			@SuppressWarnings("deprecation")
			@Override
			public void mouseClicked(MouseEvent e) {
				User user;
	          try {
	        	  username=txtUsername.getText();
	        	 password=pwdPass.getText();
	        	 System.out.println(username);
	        	 System.out.println(password);
	        	       user= connection.fingByUsername(username);
	        	       
	        	        if(user.getClass()==Client.class ){
	        	        	if(user.getPassword().equals(password)){
	    		        		appUILoged loged=new appUILoged(con,username);
	    		        		loged.setVisible(true);
	    		        		setVisible(false);
	    		        		
	  		        	  }
	        	        }else
	        	        if(user.getClass()== Administrator.class  ){
	        				
		        	        	if(user.getPassword().equals(password)){
		    		        	appAdminUI	aui=new  appAdminUI(con,username);
		    						aui.setVisible(true);
		    						setVisible(false);
		    						
		        	     
		  		        	  }
					   
	        		    }else
	        	        {
		        	        JOptionPane.showMessageDialog(null, "User or password is inncorect");
		        	        }
	        		  
	        	        }  catch (Exception e1) {
					e1.printStackTrace();
					
				}
	          
			}
			
		});
		
		
		
		
		lblUserName = new JLabel("User name:");
		lblUserName.setBounds(80, 40, 80, 14);
		logPane.add(lblUserName);
		
		btnAutentification.setBounds(100, 120, 200, 23);
		logPane.add(btnAutentification);
		
		

		
		txtUsername = new JTextField();
		txtUsername.setBounds(160, 30, 110, 30);
		logPane.add(txtUsername);
		txtUsername.setColumns(10);
		
		lblPassword = new JLabel("Password:");
		lblPassword.setBounds(80, 80, 80, 14);
		logPane.add(lblPassword);
		
		pwdPass = new JPasswordField();
		pwdPass.setText("");
		pwdPass.setBounds(160, 70, 110, 30);
		logPane.add(pwdPass);
		
		btnBack = new JButton("back");
		btnBack.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				logPane.setVisible(false);
				try {
					home(con);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnBack.setBounds(160, 180, 110, 23);
		logPane.add(btnBack);
		

	}
	
	
	private void anime(Connection con) throws SQLException{
		
		setTitle("Anime");
		//setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		animePane = new JPanel();
		animePane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(animePane);
		animePane.setLayout(null);
		
		menu(animePane,con);
		
		List list = new List();
		list.setBounds(10, 50, 110, 190);
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					System.out.println(list.getSelectedItem());
					try {
						Anime a=animeDB.findByName(list.getSelectedItem(),con);
						animePane.setVisible(false);
						selectedAnime(a,con);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				  }
				
			}
		});
		getList(list,con);
		
		animePane.add(list);
	}
	
    private void getList(List list,Connection con) throws SQLException{
		
		for(Anime a :animeDB.getAnimes(con)){
			list.add(a.getName());
		}
		
	}
	
	private void selectedAnime(Anime anime,Connection con) throws SQLException{
		setTitle("Anime");
    	setBounds(100, 100, 450, 330);
		selectedAnimePane = new JPanel();
		selectedAnimePane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(selectedAnimePane);
		selectedAnimePane.setLayout(null);
		
		menu(selectedAnimePane,con);
		
		JLabel lblAnime = DefaultComponentFactory.getInstance().createTitle(anime.getName());
		lblAnime.setBounds(10,45, 88, 14);
		selectedAnimePane.add(lblAnime);
		
		
		
		List list = new List();
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					System.out.println(list.getSelectedItem());
					Episode episode;
					try {
						episode = animeDB.findEpisodeByName(list.getSelectedItem(),con);
						selectedAnimePane.setVisible(false);
						getEpisode(episode,con,anime.getName());
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				  }
				
			}
		});
		list.setBounds(10, 100, 120, 165);
		getList(list,anime);
		
	 
		
		selectedAnimePane.add(list);
		
		 lblScore = new JLabel("");
		lblScore.setBounds(150, 145, 97, 14);
		lblScore.setText("Score: "+anime.getRating());
		selectedAnimePane.add(lblScore);
		
		
		JCheckBox chckbxFollow = new JCheckBox("Follow");
		for(Anime a :animeDB.getUserAnime(username,con)){
			if(a.getName().equals(anime.getName())){
				chckbxFollow.setSelected(true);
			}
		}
		chckbxFollow.setBounds(150, 165, 97, 23);
		
		selectedAnimePane.add(chckbxFollow);
		
		
       
		
		
		
		JLabel lblGen = new JLabel(getGen(anime));
		lblGen.setBounds(10, 55, 264, 39);
		selectedAnimePane.add(lblGen);
		
		if(anime.getR()){
		JLabel lblR = new JLabel("R");
		lblR.setBounds(150, 120, 46, 14);
		selectedAnimePane.add(lblR);
		}		
		
	}
	
	
	
	
	private void getList(List list,Anime a) throws SQLException{
		for(Episode e :a.getEpsisodes()){
			list.add(e.getName());
			
		}
		}
	
	private String getGen(Anime a){
		String gen="Gen: ";
		for(Gen g:a.getGens()){
			gen+=g.name().toLowerCase()+",";
		}
		return gen;
	}
	
	private void getEpisode(Episode ep,Connection con,String anime){
		setTitle("Episode");
	//	setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 640, 500);
		
		episodePane = new JPanel();
		episodePane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(episodePane);
		episodePane.setLayout(null);
		menu(episodePane,con);
		JLabel lblName = new JLabel(ep.getName());
		lblName.setBounds(10, 45, 80, 14);
		episodePane.add(lblName);
  
		 //set name si img in fct de ep
		EpisodeFactory epBack=new EpisodeFactory();
		EpisodeDecorator decorator= epBack.selectEpisode(anime);
		ImageIcon img=decorator.background();

		JLabel lblEp = new JLabel("");
		lblEp.setIcon(img);		
		lblEp.setBounds(10, 60, 600, 400);	
		episodePane.add(lblEp);
	}

	
	
}
