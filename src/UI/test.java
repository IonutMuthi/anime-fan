package UI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JCheckBox;

public class test extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					test frame = new test();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public test() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(10, 39, 71, 14);
		contentPane.add(lblName);
		
		textField = new JTextField();
		textField.setBounds(100, 18, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		lblName.setBounds(10, 21, 71, 14);
		
		
		
		JButton btnAddAnime = new JButton("Add anime");
		btnAddAnime.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
			}
		});
		btnAddAnime.setBounds(10, 227, 116, 23);
		contentPane.add(btnAddAnime);
		
		textField_2 = new JTextField();
		textField_2.setBounds(300, 18, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblEpisodeName = new JLabel("Episode name:");
		lblEpisodeName.setBounds(205, 21, 85, 14);
		contentPane.add(lblEpisodeName);
		
		JLabel lblAnimeName = new JLabel("Anime name:");
		lblAnimeName.setBounds(205, 60, 86, 14);
		contentPane.add(lblAnimeName);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(300, 57, 86, 20);
		contentPane.add(textField_3);
		
		JButton btnAddEpisode = new JButton("Add episode");
		btnAddEpisode.setBounds(270, 140, 116, 23);
		contentPane.add(btnAddEpisode);
		
		JCheckBox chckbxAction = new JCheckBox("Action");
		chckbxAction.setBounds(100, 84, 71, 23);
		contentPane.add(chckbxAction);
		
		JLabel lblGen = new JLabel("Gen:");
		lblGen.setBounds(10, 88, 46, 14);
		contentPane.add(lblGen);
		
		JCheckBox chckbxAdventure = new JCheckBox("Adventure");
		chckbxAdventure.setBounds(100, 110, 97, 23);
		contentPane.add(chckbxAdventure);
		
		JCheckBox chckbxComedy = new JCheckBox("Comedy");
		chckbxComedy.setBounds(100, 140, 97, 23);
		contentPane.add(chckbxComedy);
		
		JCheckBox chckbxDrama = new JCheckBox("Drama");
		chckbxDrama.setBounds(10, 110, 97, 23);
		contentPane.add(chckbxDrama);
		
		JCheckBox chckbxFantasy = new JCheckBox("Fantasy");
		chckbxFantasy.setBounds(10, 140, 97, 23);
		contentPane.add(chckbxFantasy);
		
		JCheckBox chckbxHorror = new JCheckBox("Horror");
		chckbxHorror.setBounds(100, 166, 97, 23);
		contentPane.add(chckbxHorror);
		
		JCheckBox chckbxMecha = new JCheckBox("Mecha");
		chckbxMecha.setBounds(10, 166, 97, 23);
		contentPane.add(chckbxMecha);
		
		JCheckBox chckbxRomance = new JCheckBox("Romance");
		chckbxRomance.setBounds(100, 192, 97, 23);
		contentPane.add(chckbxRomance);
		
		JCheckBox chckbxMystery = new JCheckBox("Mystery");
		chckbxMystery.setBounds(10, 192, 97, 23);
		contentPane.add(chckbxMystery);
		
		JCheckBox chckbxR = new JCheckBox("R");
		chckbxR.setBounds(6, 56, 97, 23);
		contentPane.add(chckbxR);
		
		
	}
}
