package UI;


import java.awt.EventQueue;
import java.awt.List;

import javax.swing.ImageIcon;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import javax.swing.JRadioButton;

import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;


import dao.AnimeDAOImplement;

import daoInterface.AnimeDAO;
import decorator.EpisodeDecorator;
import factory.EpisodeFactory;
import model.Anime;

import model.Episode;
import model.Gen;

import javax.swing.JMenuBar;
import javax.swing.JMenu;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Observable;
import java.util.Observer;

public class appUILoged extends JFrame  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel homePane;

	private JPanel animePane;
	private JPanel selectedAnimePane;
	private JPanel episodePane;
	
	
	

	private JLabel lblScore;
    
    private AnimeDAO animeDB=new AnimeDAOImplement();
	


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					String local=null;
					appUILoged frame = new appUILoged(null, local);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param con 
	 * @throws SQLException 
	 */
	public appUILoged(Connection con,String username) throws SQLException {
	     home(con,username);
	}
	
	
	private void menu(JPanel panel,Connection con,String username){
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 434, 40);
		panel.add(menuBar);
		
		JMenu mnHome = new JMenu("Home");
		mnHome.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				panel.setVisible(false);
				try {
					home(con,username);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		menuBar.add(mnHome);
		
		JMenu mnAnimeList = new JMenu("Anime List");
		mnAnimeList.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					panel.setVisible(false);
					anime(con,username);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		menuBar.add(mnAnimeList);
		
		JMenu mnLogIn = new JMenu(username);
		
		menuBar.add(mnLogIn);
		
	
	}
	
	private void home(Connection con, String username) throws SQLException{
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Anime fan");
		setBounds(100, 100, 450, 300);
		homePane = new JPanel();
		homePane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		setContentPane(homePane);
		homePane.setLayout(null);
		
		menu(homePane,con, username);
		
		List list = new List();
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					System.out.println(list.getSelectedItem());
					Episode episode;
					try {
						episode = animeDB.findEpisodeByName(list.getSelectedItem(),con);
						homePane.setVisible(false);
						getEpisode(episode,con, username,animeDB.findByEpisode(episode.getName(), con).getName());
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				  }
				
			}
		});
		list.setBounds(10, 50, 110, 190);
		getLast5(list,con);

		homePane.add(list);
		List list2 = new List();
		list2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					System.out.println(list.getSelectedItem());
					Anime anime;
					try {
						anime = animeDB.findByName(list2.getSelectedItem(),con);
						homePane.setVisible(false);
						selectedAnime(anime,con, username);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				  }
				
			}

			
		});
		list2.setBounds(150, 50, 110, 190);
		getAnimes(list2,con,username);

		homePane.add(list2);

	}
	
	private void getAnimes(List list, Connection con, String username) throws SQLException {
		for(Anime a :animeDB.getUserAnime(username, con)){
			list.add(a.getName());
		}
		
	}
	
    private void getLast5(List list,Connection con) throws SQLException{
		
		for(Episode a :animeDB.getLast5Episodes(con)){
			list.add(a.getName());
		}
		
	}
	
	
	
	

	private void anime(Connection con,String username) throws SQLException{
		
		setTitle("Anime");
		//setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		animePane = new JPanel();
		animePane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(animePane);
		animePane.setLayout(null);
		
		menu(animePane,con,username);
		
		List list = new List();
		list.setBounds(10, 50, 110, 190);
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					System.out.println(list.getSelectedItem());
					try {
						Anime a=animeDB.findByName(list.getSelectedItem(),con);
						animePane.setVisible(false);
						selectedAnime(a,con,username);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				  }
				
			}
		});
		getList(list,con);
		
		animePane.add(list);
	}
	
    private void getList(List list,Connection con) throws SQLException{
		
		for(Anime a :animeDB.getAnimes(con)){
			list.add(a.getName());
		}
		
	}
	
	private void selectedAnime(Anime anime,Connection con,String username) throws SQLException{
		setTitle("Anime");
	//	setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 330);
		selectedAnimePane = new JPanel();
		selectedAnimePane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(selectedAnimePane);
		selectedAnimePane.setLayout(null);
		
		menu(selectedAnimePane,con,username);
		
		JLabel lblAnime = DefaultComponentFactory.getInstance().createTitle(anime.getName());
		lblAnime.setBounds(10,45, 88, 14);
		selectedAnimePane.add(lblAnime);
		
		
		
		List list = new List();
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					System.out.println(list.getSelectedItem());
					Episode episode;
					try {
						episode = animeDB.findEpisodeByName(list.getSelectedItem(),con);
						selectedAnimePane.setVisible(false);
						getEpisode(episode,con, username,anime.getName());
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				  }
				
			}
		});
		list.setBounds(10, 100, 120, 165);
		getList(list,anime);
		
	 
		
		selectedAnimePane.add(list);
		
		 lblScore = new JLabel("");
		lblScore.setBounds(150, 145, 97, 14);
		lblScore.setText("Score: "+anime.getRating());
		selectedAnimePane.add(lblScore);
		
		
		JCheckBox chckbxFollow = new JCheckBox("Follow");
		for(Anime a :animeDB.getUserAnime(username,con)){
			if(a.getName().equals(anime.getName())){
				chckbxFollow.setSelected(true);
			}
		}
		chckbxFollow.setBounds(150, 165, 97, 23);
		
		selectedAnimePane.add(chckbxFollow);
		
		rate(anime,username,con);
       
		
		JLabel lblRate = new JLabel("  1    2     3    4     5");
		lblRate.setBounds(150, 215, 101, 14);
		selectedAnimePane.add(lblRate);
		
		JLabel lblGen = new JLabel(getGen(anime));
		lblGen.setBounds(10, 55, 264, 39);
		selectedAnimePane.add(lblGen);
		
		if(anime.getR()){
		JLabel lblR = new JLabel("R");
		lblR.setBounds(150, 120, 46, 14);
		selectedAnimePane.add(lblR);
		}		
		
	}
	
	
	private void rate(Anime anime,String username,Connection con){
		JRadioButton rdbtnS1 = new JRadioButton("");   // combo box better ? 
		JRadioButton rdbtnS2 = new JRadioButton("");
		JRadioButton rdbtnS3 = new JRadioButton("");
		JRadioButton rdbtnS4 = new JRadioButton("");
		JRadioButton rdbtnS5 = new JRadioButton("");
		
		rdbtnS1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				anime.setRate(1);
				rdbtnS2.setSelected(false);
				rdbtnS3.setSelected(false);
				rdbtnS4.setSelected(false);
				rdbtnS5.setSelected(false);
				lblScore.setText("Score: "+anime.getRating());
				try {
					
					animeDB.updateRating(anime.getName(),  Double.parseDouble(anime.getRating()), con);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		rdbtnS1.setBounds(150, 190, 21, 23);
		selectedAnimePane.add(rdbtnS1);
		
		
		rdbtnS2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				rdbtnS1.setSelected(true);
				rdbtnS3.setSelected(false);
				rdbtnS4.setSelected(false);
				rdbtnS5.setSelected(false);
				lblScore.setText("Score: "+anime.getRating());
				anime.setRate(2);
				try {
					animeDB.updateRating(anime.getName(),  Double.parseDouble(anime.getRating()), con);
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		rdbtnS2.setBounds(170, 190, 21, 23);
		selectedAnimePane.add(rdbtnS2);
		
	
		rdbtnS3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				anime.setRate(3);
				rdbtnS1.setSelected(true);
				rdbtnS2.setSelected(true);
				rdbtnS4.setSelected(false);
				rdbtnS5.setSelected(false);
				lblScore.setText("Score: "+anime.getRating());
				try {
					animeDB.updateRating(anime.getName(),  Double.parseDouble(anime.getRating()), con);
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		rdbtnS3.setBounds(190, 190, 21, 23);
		selectedAnimePane.add(rdbtnS3);
		
		
		rdbtnS4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				rdbtnS1.setSelected(true);
				rdbtnS2.setSelected(true);
				rdbtnS3.setSelected(true);
				rdbtnS5.setSelected(false);
				anime.setRate(4);
				lblScore.setText("Score: "+anime.getRating());
				try {
					animeDB.updateRating(anime.getName(),  Double.parseDouble(anime.getRating()), con);
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		rdbtnS4.setBounds(210, 190, 21, 23);
		selectedAnimePane.add(rdbtnS4);
		
		
		rdbtnS5.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				rdbtnS1.setSelected(true);
				rdbtnS2.setSelected(true);
				rdbtnS3.setSelected(true);
				rdbtnS4.setSelected(true);
				anime.setRate(5);
				lblScore.setText("Score: "+anime.getRating());
				try {
					animeDB.updateRating(anime.getName(), Double.parseDouble(anime.getRating()), con);
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		rdbtnS5.setBounds(230, 190, 21, 23);
		selectedAnimePane.add(rdbtnS5);
	}
	
	private void getList(List list,Anime a) throws SQLException{
		for(Episode e :a.getEpsisodes()){
			list.add(e.getName());
			
		}
		}
	
	private String getGen(Anime a){
		String gen="Gen: ";
		for(Gen g:a.getGens()){
			gen+=g.name().toLowerCase()+",";
		}
		return gen;
	}
	
	private void getEpisode(Episode ep,Connection con,String username,String anime){
		setTitle("Episode");
	//	setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 640, 500);
		
		episodePane = new JPanel();
		episodePane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(episodePane);
		episodePane.setLayout(null);
		menu(episodePane,con,username);
		JLabel lblName = new JLabel(ep.getName());
		lblName.setBounds(10, 45, 80, 14);
		episodePane.add(lblName);
  
		 //set name si img in fct de ep

		EpisodeFactory epBack=new EpisodeFactory();
		EpisodeDecorator decorator= epBack.selectEpisode(anime);
		ImageIcon img=decorator.background();
		
		JLabel lblEp = new JLabel("");
		lblEp.setIcon(img);		
		lblEp.setBounds(10, 60, 600, 400);	
		episodePane.add(lblEp);
	}

	

	
	
}
