package daoInterface;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Anime;
import model.Episode;
import model.Gen;

public interface AnimeDAO {
	
	Anime findByName(String name,Connection con) throws SQLException;
	void updateRating(String name,double rate,Connection con) throws SQLException;
	Anime findById(int id,Connection con) throws SQLException;
	Anime findByEpisode(String name,Connection con)throws SQLException;
	Episode findEpisodeByName(String name,Connection con) throws SQLException;
	ArrayList<Anime> getUserAnime(String username,Connection con) throws SQLException;
	ArrayList<Anime> getAnimes(Connection con) throws SQLException;
	ArrayList<Gen> getGen(int animeid,Connection con) throws SQLException;
	ArrayList<Episode> getEpisodes(int animeid,Connection con) throws SQLException;
	ArrayList<Episode> getLast5Episodes(Connection con) throws SQLException;
	
	void insertEpisode(String a,String e,Connection con) throws SQLException;
	void insertAnime(Anime a,Connection con) throws SQLException;
	void deleteAnime(String name,Connection con)  throws SQLException;
	void updateAnime(Anime a,Connection con)  throws SQLException;

}
