package daoInterface;

import java.sql.SQLException;
import java.util.ArrayList;
import model.Administrator;
import model.Client;
import model.User;

public interface UserDAO {

	User fingByUsername(String username) throws SQLException;
	ArrayList<Client> getClietns() throws SQLException;
	ArrayList<Administrator> getAdmins() throws SQLException;
	
	void insertClient(User u)throws SQLException;
	void insertAdmin(User u)throws SQLException;
	void deleteUser(String username)throws SQLException;
	void updateUser(User u) throws SQLException;
	
}
