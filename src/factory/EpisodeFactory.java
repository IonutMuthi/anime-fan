package factory;

import decorator.*;

public class EpisodeFactory {
    
	public EpisodeDecorator selectEpisode(String ep){
		if(ep.equals("overlord")){
			EpisodeOverlord eo=new EpisodeOverlord();
			return eo;
		}
		if(ep.equals("naruto")){
			EpisodeNaruto en=new EpisodeNaruto();
			return en;
		}
		if(ep.equals("91Days")){
			Episode91Days day=new Episode91Days();
			return day;
		}
		if(ep.equals("Tokyo Goul")){
			EpisodeTokyo tokyo=new EpisodeTokyo();
			return tokyo;
		}
		if(ep.equals("Boruto")){
			EpisodeBoruto br=new EpisodeBoruto();
			return br;
		}
		if(ep.equals("Nsnstdu no Taizai")){
			EpisodeNsnstduNoTaizai seven=new EpisodeNsnstduNoTaizai();
			return seven;
		}
		if(ep.equals("Akame ga Kill!")){
			EpisodeAkame ak=new EpisodeAkame();
			return ak;
		}
		return null;
		
		
	}
	
	
}
