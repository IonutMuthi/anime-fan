package strategy;
import java.util.*;  
import javax.mail.*;  
import javax.mail.internet.*;  
import javax.activation.*;  
  
public class SendEmail {  
	
 public  void sendMail(String anime){  
	  try{ 
	 String to = "****";//user mail
     String from = "***";//admin mail
     String subject = "New anime";
     String host ="smtp.gmail.com" ;
     String user = "*****";//mail user
     String pass = "****"; //parola email
     boolean sessionDebug = false;
     //Get the session object  
     Properties props = System.getProperties();

     props.put("mail.smtp.starttls.enable", "true");
     props.put("mail.smtp.host", host);
     props.put("mail.smtp.port", "587");
     props.put("mail.smtp.auth", "true");
     props.put("mail.smtp.starttls.required", "true");
     
      
  
     //compose the message  
     
     String messageText="We just added "+anime + " you might like it.";
          Session mailSession = Session.getDefaultInstance(props, null);
          mailSession.setDebug(sessionDebug);
          Message msg = new MimeMessage(mailSession);
          msg.setFrom(new InternetAddress(from));
          InternetAddress[] address = {new InternetAddress(to)};
          msg.setRecipients(Message.RecipientType.TO, address);
          msg.setSubject(subject); msg.setSentDate(new Date());
          msg.setText(messageText);

         Transport transport=mailSession.getTransport("smtp");
         transport.connect(host, user, pass);
         transport.sendMessage(msg, msg.getAllRecipients());
         transport.close();
         System.out.println("message send successfully");
  
      }catch (MessagingException mex) {mex.printStackTrace();}  
   }  
}  