package strategy;

public interface StrategyInterface {
   public void sugest(String anime);
}
