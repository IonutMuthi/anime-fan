package model;

import java.util.ArrayList;
import java.util.Observer;


public class Client extends User  {

  
    public Abonament abonamet;
    public ArrayList<Anime> following=new ArrayList<>();
    public boolean acces;

    public Client(String name,String password, String username,String email,int age){
        this.setName(name);
        this.setPassword(password);
        this.setUserame(username);
        this.setEmail(email);
        this.setAge(age);
       
        
    }

    public void choseAbonament(AbonamentTipe ab,double price){
       abonamet.pais(ab,price);

    }

    public void addAnime(Anime a){
        following.add(a);
    }

    public void removeAnime(Anime a){
        following.remove(a);
    }

    public void setAcces(){
        if(abonamet.paid==true){
            acces=true;
        }
        else{
            acces=false;
        }
    }
    public boolean getAcces(){
    	return acces;
    }
    
    public Abonament getAbonament(){
    	return abonamet;
    }


}
