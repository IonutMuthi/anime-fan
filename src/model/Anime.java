package model;

import java.util.ArrayList;


public class Anime {
    public String name;
    public boolean follow=false;
    public double rating=0;
    public int rate=0;
    public ArrayList<Gen> gens=new ArrayList<>();
    public ArrayList<Episode> episodes=new ArrayList<>();
    public boolean R;

    public Anime(String name,double rating,ArrayList<Gen> gens,boolean R,ArrayList<Episode> episodes){
        this.name=name;
        this.rating=rating;
        this.gens=gens;
        this.R=R;
        this.episodes=episodes;
    }
   
    public boolean getR(){
    	return R;
    }
    
    public void addEpisode(Episode e){  // posibil facut din dao...clar facut in dao
    	episodes.add(e);
    }

    public void removeEpisode(Episode e){  // posibil facut din dao...clar facut in dao
    	episodes.remove(e);
    }
    

    
    public ArrayList<Episode> getEpsisodes(){   // posibil facut din dao...clar facut in dao
		return episodes;
    	
    }

    public void unFollow(){
        follow= false;
    }

    public void setFollow(){
        follow=true;
    }

    public void setRate(int r){
        rate=r;
        setRating();
    }

    public void setRating(){
        if(rating>0) {
            rating = (rating + rate) / 2;  // ratingul e media de la toate rate-urile
        }else{
            rating=rate; // daca ratingul e 0 ( nimeni nu a dat rate) atunci ia valoarea de la rete
        }

    }

    public String getName(){
        return name;
    }

    public String getRating(){
        return ""+rating;
    }
    
    public double getDRating(){
    	return rating;
    }

    public boolean getFollow(){
        return follow;
    }

    public ArrayList<Gen> getGens(){
        return gens;
    }
}
