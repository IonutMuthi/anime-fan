package model;

import java.awt.Image;

public class Episode {
      String name;
      Image episode;
      
      public Episode(String name){
    	  this.name=name;
    	 
      }
      
      public String getName(){
    	  return name;
      }
      public Image getImage(){
    	  return episode;
      }
      public void setImage(Image image){
    	  episode=image;
      }
      
}
