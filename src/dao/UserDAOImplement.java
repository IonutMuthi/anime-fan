package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import connection.ConnectionFactory;
import daoInterface.UserDAO;
import model.Administrator;
import model.Client;
import model.User;

public class UserDAOImplement implements UserDAO {

	@Override
	public User fingByUsername(String username) throws SQLException {

		Connection con = ConnectionFactory.getConnection();
		Statement myStm2=con.createStatement();
		ResultSet rs2=myStm2.executeQuery("select * from user  where username='"+username+"';");
		
		while(rs2.next()){
			System.out.println(rs2.getString("Tipe"));
			if(rs2.getString("Tipe").equals("admin")){
				
			 Administrator	toReturn=new Administrator(rs2.getString("Name"),rs2.getString("Password"),rs2.getString("Username"),rs2.getString("Email"),rs2.getInt("age"));
			  return toReturn;
		}
			else if(rs2.getString("Tipe").equals("client")){
			Client	toReturn=new Client(rs2.getString("Name"),rs2.getString("Password"),rs2.getString("Username"),rs2.getString("Email"),rs2.getInt("age"));
				return toReturn;
			}
		}
		
		
		return null;

	}
	
	public Client findClient(Client c){
		return c;
	}
	
	public Administrator findAdmin(Administrator a){
		return a;
	}

	@Override
	public ArrayList<Client> getClietns() throws SQLException {
		ArrayList<Client> clients=new ArrayList<>();
		Connection con = ConnectionFactory.getConnection();
		Statement myStm2=con.createStatement();
		ResultSet rs2=myStm2.executeQuery("select * from user");
		while(rs2.next()){
			if(rs2.getString("Tipe").equals("client")){
			Client c=new Client(rs2.getString("Name"),rs2.getString("Password"),rs2.getString("Username"),rs2.getString("Email"),rs2.getInt("age"));
			clients.add(c);
	}
		}

		ConnectionFactory.close(con);
		ConnectionFactory.close(myStm2);
		ConnectionFactory.close(rs2);
			 
		return clients;
	}
	

	@Override
	public ArrayList<Administrator> getAdmins() throws SQLException {
		ArrayList<Administrator> admins=new ArrayList<>();
		Connection con = ConnectionFactory.getConnection();
		Statement myStm2=con.createStatement();
		ResultSet rs2=myStm2.executeQuery("select * from user");
		while(rs2.next()){
			if(rs2.getString("Tipe").equals("admin")){
				Administrator a=new Administrator(rs2.getString("Name"),rs2.getString("Password"),rs2.getString("Username"),rs2.getString("Email"),rs2.getInt("age"));
			admins.add(a);
		}
		}

		ConnectionFactory.close(con);
		ConnectionFactory.close(myStm2);
		ConnectionFactory.close(rs2);
			 
		return admins;
	}
	
	@Override
	public void insertClient(User u) throws SQLException {
		// TODO Auto-generated method stub
		Connection con = ConnectionFactory.getConnection();
		Statement s=con.createStatement();
		

	String client="INSERT INTO `animedb`.`user` (`Username`, `Password`, `Name`, `Email`, `age`, `Tipe`) VALUES ('"+
				   u.getUserame()+"', '"+
			       u.getPassword()+"', '"+
			       u.getName()+"', '"+
			       u.getUserame()+"', '"+
			       u.getAge()+"', 'client');";

			
	

			s.executeUpdate(client);
			
			ConnectionFactory.close(con);
			ConnectionFactory.close(s);
			
		
	
	}
	
	@Override
	public void insertAdmin(User u) throws SQLException {
		// TODO Auto-generated method stub
		Connection con = ConnectionFactory.getConnection();
		Statement s=con.createStatement();
		

	String admin="INSERT INTO `animedb`.`user` (`Username`, `Password`, `Name`, `Email`, `age`, `Tipe`) VALUES ('"+
				   u.getUserame()+"', '"+
			       u.getPassword()+"', '"+
			       u.getName()+"', '"+
			       u.getUserame()+"', '"+
			       u.getAge()+"', 'admin');";

			
	

			s.executeUpdate(admin);
			
			ConnectionFactory.close(con);
			ConnectionFactory.close(s);
		
	}

	@Override
	public void deleteUser(String username)throws SQLException {
		Connection con = ConnectionFactory.getConnection();
		Statement s= con.createStatement();
		String user="DELETE FROM `animedb`.`user` WHERE `Username`='"+username+"';";

				
		s.executeUpdate(user);	

		ConnectionFactory.close(con);
		ConnectionFactory.close(s);
		
	}

	@Override
	public void updateUser(User u) throws SQLException {
		// TODO Auto-generated method stub
		Connection con = ConnectionFactory.getConnection();
		Statement s= con.createStatement();
		String user="UPDATE `animedb`.`user` SET', `Password`='"+u.getPassword()+
		"', `Name`='"+u.getName()+"', `Email`='"+u.getEmail()+
		"', `age`='"+u.getAge()+"' WHERE `Username`='"+u.getUserame()+"';";
		

				
			
		s.executeUpdate(user);	

		ConnectionFactory.close(con);
		ConnectionFactory.close(s);
		
		
	}



}
