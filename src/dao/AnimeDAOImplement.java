package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import connection.ConnectionFactory;
import daoInterface.AnimeDAO;
import model.Anime;
import model.Episode;
import model.Gen;


public class AnimeDAOImplement implements AnimeDAO {

	@Override
	public Anime findByName(String name,Connection con) throws SQLException {
	Anime toReturn=null;
	
	for(Anime a:getAnimes(con)){
		
		String anime=a.getName();
		
		if(anime.equals(name)){
			toReturn=new Anime(a.getName(),a.getDRating(),a.getGens(),a.getR(),a.getEpsisodes());
		
		
	}
	}
	
	return toReturn;

	}

	@Override
	public ArrayList<Anime> getUserAnime(String username,Connection con) throws SQLException {
		ArrayList<Anime> anime=new ArrayList<>();
		
		Statement myStm2=con.createStatement();
		ResultSet rs2=myStm2.executeQuery("select * from user_has_anime");
		while(rs2.next()){
			if(rs2.getString("User_Username").equals(username)){
				
			 	Anime a=findByName(rs2.getString("Anime_Name"),con);
			 	anime.add(a);
			}
		}

		
		ConnectionFactory.close(myStm2);
		ConnectionFactory.close(rs2);
			 
		return anime;
	}

	@Override
	public void insertAnime(Anime a,Connection con) throws SQLException {
		
		Statement s=con.createStatement();
		int R=0;
		if(a.getR()){
			 R=1;
		}

	String anime="INSERT INTO `animedb`.`anime` (`Name`, `Rating`, `R`) VALUES ('"+
	              a.getName()+"', '"+a.getDRating()+"', '"+R+"');";


			s.executeUpdate(anime);
			
			ConnectionFactory.close(s);
		
	}

	@Override
	public void deleteAnime(String name,Connection con) throws SQLException {
		
		Statement s= con.createStatement();
		String anime="DELETE FROM `animedb`.`anime` WHERE `Name`='"+name+"';";

				
		s.executeUpdate(anime);	

		ConnectionFactory.close(s);
		
	}

	@Override
	public void updateAnime(Anime a,Connection con) throws SQLException {
		
		Statement s= con.createStatement();
		String anime="UPDATE `animedb`.`anime` SET `Name`='"+
		a.getName()+"', `Rating`='"+a.getDRating()+"', `R`='"+a.getR()+"' WHERE `Name`='"+a.getName()+"';";
		
	
		s.executeUpdate(anime);	

	
		ConnectionFactory.close(s);
		
	}

	@Override
	public ArrayList<Anime> getAnimes(Connection con) throws SQLException {
		ArrayList<Anime> anime=new ArrayList<>();
		
		Statement myStm2=con.createStatement();
		ResultSet rs2=myStm2.executeQuery("select * from anime");
		while(rs2.next()){
			
				Anime a=new Anime(rs2.getString("Name"),rs2.getDouble("Rating"),getGen(rs2.getInt("idAnime"),con),rs2.getBoolean("R"),getEpisodes(rs2.getInt("idAnime"),con));
			    
				anime.add(a);
		
		}

		
		ConnectionFactory.close(myStm2);
		ConnectionFactory.close(rs2);
			 
		return anime;
	}

	@Override
	public ArrayList<Gen> getGen(int id,Connection con) throws SQLException {
		ArrayList<Gen> gen=new ArrayList<>();
		Statement myStm2=con.createStatement();
		ResultSet rs2=myStm2.executeQuery("select * from anime_has_gen");
		while(rs2.next()){
			Gen g = null;
			switch(rs2.getInt("Gen_idGen")){
			case 1 : g=Gen.ACTION;
			        break;
			case 2 : g=Gen.ADVENTURE;
			break;
			case 3 : g=Gen.COMEDY;
			break;
			case 4 : g=Gen.DRAMA;
			break;
			case 5 : g=Gen.FANTASY;
			break;
			case 6 : g=Gen.HORROR;
			break;
			case 7 : g=Gen.MECHA;
			break;
			case 8 : g=Gen.MYSTERY;
			break;
			case 9 : g=Gen.ROMANCE;
			break;
			case 10 : g=Gen.SCIENCE;
			}
			gen.add(g);
		
		}
      
		
		ConnectionFactory.close(myStm2);
		ConnectionFactory.close(rs2);
			 
		return gen;
	}

	@Override
	public ArrayList<Episode> getEpisodes(int animeid,Connection con) throws SQLException {
		ArrayList<Episode> episodes=new ArrayList<>();
		Statement myStm2=con.createStatement();
		ResultSet rs2=myStm2.executeQuery("select * from episode");
		while(rs2.next()){
			if(rs2.getInt("Anime_idAnime")==animeid){
			Episode e=new Episode(rs2.getString("Name"));
			episodes.add(e);
			}
		}
      
		
		ConnectionFactory.close(myStm2);
		ConnectionFactory.close(rs2);
			 
		return episodes;
	}

	public ArrayList<Episode> getEpisodes(Connection con) throws SQLException {
		ArrayList<Episode> episodes=new ArrayList<>();
		Statement myStm2=con.createStatement();
		ResultSet rs2=myStm2.executeQuery("select * from episode");
		while(rs2.next()){
            
			Episode e=new Episode(rs2.getString("Name"));
			episodes.add(e);
			
		}
      
	    ConnectionFactory.close(myStm2);
		ConnectionFactory.close(rs2);
			 
		return episodes;
	}

	
	@Override
	public Episode findEpisodeByName(String name,Connection con) throws SQLException {
		Episode toReturn=null;
		
		for(Episode e:getEpisodes(con)){
			
			String episode=e.getName();
			
			if(episode.equals(name)){
				toReturn=new Episode(e.getName());
			
			
		}
		}
		
		
		return toReturn;
	}
	
	

	@Override
	public ArrayList<Episode> getLast5Episodes(Connection con) throws SQLException {
		ArrayList<Episode> episodes=new ArrayList<>();
		Statement myStm2=con.createStatement();
		ResultSet rs2=myStm2.executeQuery("select * from animedb.episode order by idEpisode desc limit 5;");
		while(rs2.next()){

			Episode e=new Episode(rs2.getString("Name"));
			episodes.add(e);
			
		}
      
	    ConnectionFactory.close(myStm2);
		ConnectionFactory.close(rs2);
		return episodes;	 
	}

	@Override
	public Anime findByEpisode(String name, Connection con) throws SQLException {
    Anime toReturn=null;
		

		Statement myStm2=con.createStatement();
		ResultSet rs2=myStm2.executeQuery("select * from episode");
		
		while(rs2.next()){
			
			if(rs2.getString("Name").equals(name)){
				toReturn=findById(rs2.getInt("Anime_idAnime"),con);
			}
		}
      

		
		ConnectionFactory.close(myStm2);
		ConnectionFactory.close(rs2);
			 
		return toReturn;
	
	}
		
	

	@Override
	public Anime findById(int id, Connection con) throws SQLException {
		Anime toReturn=null;
		

		Statement myStm2=con.createStatement();
		ResultSet rs2=myStm2.executeQuery("select * from anime");
		while(rs2.next()){
			
				 int a=rs2.getInt("idAnime");
				if(a==id){
					toReturn=new Anime(rs2.getString("Name"),rs2.getDouble("Rating"),getGen(rs2.getInt("idAnime"),con),rs2.getBoolean("R"),getEpisodes(rs2.getInt("idAnime"),con));
					   
				}
		
		}

		
		ConnectionFactory.close(myStm2);
		ConnectionFactory.close(rs2);
			 
		return toReturn;
	
	}

	@Override
	public void updateRating(String name, double rate, Connection con) throws SQLException {
		Anime a=findByName(name,con);
		Statement s= con.createStatement();
		
		String anime="UPDATE `animedb`.`anime` SET `Rating`='"+
				rate+"' WHERE `Name`='"+a.getName()+"';";
		
	
		s.executeUpdate(anime);	

	
		ConnectionFactory.close(s);
	}

	@Override
	public void insertEpisode(String a, String ep, Connection con) throws SQLException {
		
		int id=0;
		Statement myStm2=con.createStatement();
		ResultSet rs2=myStm2.executeQuery("select * from anime");
		while(rs2.next()){
			
				 String an=rs2.getString("Name");
				if(a.equals(an)){
					id=rs2.getInt("idAnime") ;
				}
		
		}


		Statement s=con.createStatement();
    System.out.println(a+ " " +ep+" "+id);
	String episode="INSERT INTO `animedb`.`episode` (`Name`, `Anime_idAnime`) VALUES ('"+
	              ep+"', '"+id+"');";


			s.executeUpdate(episode);
			
			ConnectionFactory.close(myStm2);
			ConnectionFactory.close(rs2);
			ConnectionFactory.close(s);
		
		
	}

}
