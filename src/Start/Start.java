package Start;



import java.sql.Connection;
import java.sql.SQLException;

import UI.appUI;
import connection.ConnectionFactory;


public class Start {

	public static  int ok=1;
	
	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		Connection con = ConnectionFactory.getConnection();
		appUI log= new appUI(con);
		
		log.setVisible(true);
		
	
		
		log.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		    	ConnectionFactory.close(con);
		    }
		});
		
		
	} 
	
	
}
